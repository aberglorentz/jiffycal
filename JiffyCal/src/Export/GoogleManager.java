package Export;

// google API imports
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;

// google calendar service imports
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Calendar;     // importing the model, the service handler will be explicitly referenced
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;

import org.joda.time.*;
import org.joda.time.format.*;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.ArrayList;
import java.security.GeneralSecurityException;
import java.util.Arrays;

import Parse.SemesterSchedule;
import Parse.ClassMeeting;
import Parse.ClassMeeting.DayOfWeek;
import AppLogic.LoginException;
import java.nio.file.Files;




/**
 * Class to facilitate all Google-related export functions for the calendar.
 * 
 * @author Lorentz Aberg
 * @author Sarah Militana
 */
public class GoogleManager {
    
    private static final String APPLICATION_NAME = "JiffyCal/1.0";
    private static final String CLIENT_SECRET_FILE = "/client_secret.json";
    private static final String TIME_ZONE_STRING = "America/New_York";
    
    private static HttpTransport httpTransport;
    private static FileDataStoreFactory dataStoreFactory;
    private static com.google.api.services.calendar.Calendar calendarClient;
    
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final java.io.File DATA_STORE_DIR = new java.io.File(System.getProperty("user.home"), ".store/JiffyCal");
    private static final java.io.File DATA_STORE_FILE = new java.io.File(System.getProperty("user.home"), ".store/JiffyCal/StoredCredential");
    
    /**
     * The sole entry point for the export operation. 
     * Exports a given schedule to Google Calendar
     * 
     * @param schedule the schedule object to be exported
     * @param calendarName the desired name of the calendar in Google Calendar
     * @param username the Lehigh username - used to prevent collisions with 
     *                 Google credentials in case the file didn' get deleted or
     *                 multiple instances of the application are running
     * @throws LoginException thrown if there is a problem with accessing the Google authentication service
     * @throws IOException if there is a technical problem connecting to the Google API
     */
    public static void ExportCalendar (SemesterSchedule schedule, String calendarName, String username) throws LoginException, IOException {
        
        com.google.api.services.calendar.Calendar calendarService = getCalendarService(username);
        
        Calendar newCalendar = new Calendar();
        newCalendar.setSummary(calendarName);
        newCalendar.setDescription("Lehigh class schedule for " + SemesterSchedule.seasonToString(schedule.getSeason()) + " " + schedule.getYear());
        newCalendar.setTimeZone("America/New_York");
        
        Calendar scheduleCalendar = calendarService.calendars().insert(newCalendar).execute();
        
        HashMap<DayOfWeek, ArrayList<ClassMeeting>> classes = schedule.getClasses();
        
        int i = 1;  // integer iterator used to address days of the week within joda
        for (DayOfWeek day: DayOfWeek.values()) {
            ArrayList<ClassMeeting> dayClasses = classes.get(day);
            
            // calculate the first day of classes on this weekday
            
            
            for (ClassMeeting meeting: dayClasses) {
                // determine first day of class
                LocalDate firstMeeting = meeting.firstDay.dayOfWeek().setCopy(i);
                if (firstMeeting.isBefore(meeting.firstDay)) {
                    firstMeeting = firstMeeting.plusWeeks(1); // if setting the day of the week put the date before the first day of classes, advance the date one week
                }
                
                Event newMeeting = new Event();
                newMeeting.setSummary(meeting.className + "(" + meeting.classNumber + ")");
                
                // build event description
                StringBuilder description = new StringBuilder();
                description.append("Section ").append(meeting.classSection).append("\n");
                description.append("Instructor: ").append(meeting.professorName);
                // if there is no email, don't add one
                if (!meeting.professorEmail.isEmpty()) {
                    description.append("(").append(meeting.professorEmail).append(")").append("\n");
                }
                newMeeting.setDescription(description.toString());
                
                newMeeting.setLocation(meeting.buildingName + " " + meeting.roomNumber);
                
                // get concrete datetime objects from first day of class + class meeting time
                DateTime startTime = firstMeeting.toDateTime(meeting.startTime, DateTimeZone.forID(TIME_ZONE_STRING));
                DateTime endTime = firstMeeting.toDateTime(meeting.endTime, DateTimeZone.forID(TIME_ZONE_STRING));
                
                // convert joda time to google-readable format
                EventDateTime gStartTime = new EventDateTime();
                EventDateTime gEndTime = new EventDateTime();
                
                com.google.api.client.util.DateTime tempStart, tempEnd;
                tempStart = new com.google.api.client.util.DateTime(startTime.getMillis());
                tempEnd = new com.google.api.client.util.DateTime(endTime.getMillis());
                
                // if time APIs spent a little more time on constructors, this would be a lot easier for the end developer
                // as-is, everything has to be created as a com.google.api.client.util.DateTime and then passed into the final constructor
                gStartTime.setDateTime(tempStart);
                gEndTime.setDateTime(tempEnd);
                gStartTime.setTimeZone(TIME_ZONE_STRING);
                gEndTime.setTimeZone(TIME_ZONE_STRING);
                
                newMeeting.setStart(gStartTime);
                newMeeting.setEnd(gEndTime);
                
                // if the start time is midnight (AKA class meeting time is TBA), create event as an all-day event
                if (meeting.startTime.isEqual(LocalTime.MIDNIGHT)) {
                    newMeeting.setEndTimeUnspecified(true);
                }
                
                // build string for recurring event spec (nasty)
                StringBuilder recurrence = new StringBuilder();
                DateTime lastDay = meeting.lastDay.plusDays(1).toDateTime(LocalTime.MIDNIGHT, DateTimeZone.forID("UTC"));
                lastDay = lastDay.plusHours(1);
                
                recurrence.append("RRULE:FREQ=WEEKLY;UNTIL=");
                DateTimeFormatter fmt1 = DateTimeFormat.forPattern("YYYYMMdd");
                DateTimeFormatter fmt2 = DateTimeFormat.forPattern("HHmmss");
                recurrence.append(fmt1.print(lastDay)).append("T").append(fmt2.print(lastDay)).append("Z");
                
                // set recurrence string
                newMeeting.setRecurrence(Arrays.asList(recurrence.toString()));
                
                // add new recurring class to the calendar
                calendarService.events().insert(scheduleCalendar.getId(), newMeeting).execute();
                
            }
            i++;    // increment weekday count to facilitate joda day-of-week searching
        }
        
        // delete credentials file to force reauthentication every time
        Files.delete(DATA_STORE_FILE.toPath());
    }
    
    /**
     * Authorizes the user's Google data and creates a new calendar service object
     * @param username the Lehigh username - used to prevent collisions with 
     *                 Google credentials in case the file didn't get deleted or
     *                 multiple instances of the application are running
     * @return A new Calendar service object connected to the user's Google account
     * @throws LoginException thrown if there is a problem with authorizing the user's Google data
     */
    private static com.google.api.services.calendar.Calendar getCalendarService (String username) throws LoginException {
        
        try {
            httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);
            Credential credential = authorize(username);    // local authorization method
            
            calendarClient = new com.google.api.services.calendar.Calendar.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
        }
        catch (GeneralSecurityException e) {
            throw new LoginException("Could not establish http transport", e);
        }
        catch (IOException e) {
            throw new LoginException("Could not authenticate Google credentials", e);
        }
        
        return calendarClient;
    }
    
    /**
     * Handles loading the Google client secrets and initial JSON setup. Handles actually contacting
     * Google services for the login information, and sychronously waits for authorization
     * @param username the Lehigh username - used to prevent collisions with 
     *                 Google credentials in case the file didn't get deleted or
     *                 multiple instances of the application are running
     * @return Credential object to be used 
     * @throws LoginException if the login authorization fails
     * @throws IOException If the authorization code flow fails for any reason
     */
    private static Credential authorize (String username) throws LoginException, IOException{
        
        // load client secrets
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(GoogleManager.class.getResourceAsStream(CLIENT_SECRET_FILE)));        
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport,
                                                                                   JSON_FACTORY,
                                                                                   clientSecrets,
                                                                                   Collections.singleton(CalendarScopes.CALENDAR)
                                                                                  ).setDataStoreFactory(dataStoreFactory).build();
        
        return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize(username);        // "username" is an index into the credentials file
    }
    
}
