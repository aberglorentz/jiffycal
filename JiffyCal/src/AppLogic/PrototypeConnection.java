/*
This is a prototype, proof-of-concept program to access Banner resources. 

DO NOT REFERENCE THIS FILE FROM OTHER CODE
*/

package AppLogic;

import java.io.*;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.net.ssl.HttpsURLConnection;
import java.net.HttpURLConnection;

public class PrototypeConnection {
    
    private List<String> cookies;
    private HttpURLConnection connection;
    
    private static final String USER_AGENT = "Mozilla/5.0";
    private static final String LOGIN_URL = "https://cp5.cc.lehigh.edu/login/";
    private static final String AUTH_URL = "https://cp5.cc.lehigh.edu/cp/home/login";
    private static final String REDIRECT_URL = "http://cp5.cc.lehigh.edu/cp/home/next";
    private static final String BANNER_URL = "https://lewisweb.cc.lehigh.edu/PROD/twbkwbis.P_GenMenu?name=bmenu.P_MainMnu&msg=WELCOME";
    
    public static void main(String[] args) throws Exception {
        
        Scanner input = new Scanner(System.in);
        System.out.print("Enter username: ");
        String username = input.nextLine();
        System.out.print("Enter password: ");
        String password = input.nextLine();
        
        PrototypeConnection http = new PrototypeConnection();
        
        CookieHandler.setDefault(new CookieManager());
        
        String postParameters = http.getFormParams(username, password);
        
        http.sendPost(AUTH_URL, postParameters);
        
        // get authentication cookies from Portal
        System.out.println("Logging into Portal...");
        String result = http.getPageContent(REDIRECT_URL, "http://cp5.cc.lehigh.edu/cps/welcome/loginok.html", "cp5.cc.lehigh.edu");
        result = http.getPageContent("http://cp5.cc.lehigh.edu/render.userLayoutRootNode.uP?uP_root=root", REDIRECT_URL, "cp5.cc.lehigh.edu");
        
        // access banner button URL 
        System.out.println("Obtaining Banner session...");
        result = http.getPageContent("http://cp5.cc.lehigh.edu/cp/ip/timeout?sys=sctssb&url=https://lewisweb.cc.lehigh.edu/PROD/twbkwbis.P_GenMenu?name=bmenu.P_MainMnu", 
                                     "http://cp5.cc.lehigh.edu/render.userLayoutRootNode.uP?uP_root=root",
                                     "cp5.cc.lehigh.edu");
        
        // extract redirect URL from javascript response
        String redirectUrl = result.substring(result.indexOf("https")).split("\"")[0];
        
        System.out.println("Banner redirect URL: " + redirectUrl);
        
        result = http.getPageContent(redirectUrl, "http://cp5.cc.lehigh.edu/cp/ip/timeout?sys=sctssb&url=https://lewisweb.cc.lehigh.edu/PROD/twbkwbis.P_GenMenu?name=bmenu.P_MainMnu",
                                     "lewisweb.cc.lehigh.edu");
        
        System.out.println("\nAccessing Banner...");
        
        result = http.getPageContent("https://lewisweb.cc.lehigh.edu/PROD/bwskfshd.P_CrseSchd",
                                     "https://lewisweb.cc.lehigh.edu/PROD/twbkwbis.P_GenMenu?name=bmenu.P_MainMnu&msg=WELCOME",
                                     "lewisweb.cc.lehigh.edu");
        System.out.println("\nSchedule HTML retrieved below:\n============================================\n\n");
        System.out.println(result);
    }
    
    private void sendPost(String url, String postParams) throws Exception {
        
        URL serviceUrl = new URL(url);
        connection = (HttpsURLConnection) serviceUrl.openConnection();
        
        // set up request parameters and environment
        connection.setUseCaches(false);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Host", "cp5.cc.lehigh.edu");
        connection.setRequestProperty("User-Agent", USER_AGENT);
        connection.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        if (cookies != null) {
            for (String cookie: this.cookies) {
                connection.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
            }
        }
        connection.setRequestProperty("Connection", "keep-alive");
        connection.setRequestProperty("Referer", "https://cp5.cc.lehigh.edu/login/");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty("Content-Length", Integer.toString(postParams.length()));
        
        connection.setDoOutput(true);
        connection.setDoInput(true);
        
        // actually send the damn thing
        DataOutputStream writeStream = new DataOutputStream(connection.getOutputStream());
        writeStream.writeBytes(postParams);
        writeStream.flush();
        writeStream.close();
        
        int responseCode = connection.getResponseCode();
        System.out.println("\nSending POST request to: " + url);
        System.out.println("PostParams: " + postParams);
        System.out.println("Response Code: " + responseCode);
        
        BufferedReader responseReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        
        while ((inputLine = responseReader.readLine()) != null) {
            response.append(inputLine);
        }
        responseReader.close();
        
        setCookies(connection.getHeaderFields().get("Set-Cookie"));
        
        System.out.println(response.toString());
    }
    
    private String getPageContent(String url, String refererUrl, String hostUrl) throws Exception {
        
        URL pageUrl = new URL(url);
        connection = (HttpURLConnection) pageUrl.openConnection();
        
        // set up request parameters and environment
        connection.setRequestMethod("GET");
        connection.setUseCaches(false);
        connection.setRequestProperty("Host", hostUrl);
        connection.setRequestProperty("User-Agent", USER_AGENT);
        connection.setRequestProperty("Accept", "text/html,application/xhtml+xml,applicatoin/xml;q=0.9,*/*;q=0.8");
        connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        connection.setRequestProperty("Referer", refererUrl);
        connection.setDoOutput(true);
        connection.setRequestProperty("Connection", "keep-alive");
        if (cookies != null) {
            for (String cookie: this.cookies) {
                connection.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
            }
        }
        
        System.out.println("\nSending GET request to: " + url);
//        int responseCode = connection.getResponseCode();
        
//        System.out.println("Response Code: " + responseCode);
        
        BufferedReader responseReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        
        while ((inputLine = responseReader.readLine()) != null) {
            response.append(inputLine);
        }
        responseReader.close();
        
        setCookies(connection.getHeaderFields().get("Set-Cookie"));
        
        return response.toString();
    }
    
    public String getFormParams(String username, String password) throws UnsupportedEncodingException {
        
        System.out.println("Building form parameters...");
        
        List<String> paramList = new ArrayList<String>();
        
        // parameters hardcoded for the moment (quick & dirty)
        paramList.add("pass=" + URLEncoder.encode(password, "UTF-8"));
        paramList.add("user=" + URLEncoder.encode(username, "UTF-8"));
        paramList.add("uuid=" + URLEncoder.encode("0xACA021", "UTF-8"));
        
        // build form parameters
        StringBuilder paramString = new StringBuilder();
        for (String parameter: paramList) {
            if (paramString.length() == 0) {
                paramString.append(parameter);
            }
            else {
                paramString.append("&" + parameter);
            }
        }
        
        return paramString.toString();
    }
    
    public List<String> getCookies() {
        return cookies;
    }
    
    public void setCookies(List<String> cookies) {
        if (this.cookies == null) {
            this.cookies = new ArrayList<>();
        }
        
        if (cookies != null) {
            for (String cookieString: cookies) {
                this.cookies.add(cookieString);
            }
        }
    }
}
