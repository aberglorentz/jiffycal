package AppLogic;

import Interface.LehighLoginDialog;

import Scrape.BannerFetcher;
import Scrape.BannerSession;

import java.io.IOException;

/**
 * The main file for the JiffyCal application.
 * The contents are currently test code
 * @author Lorentz Aberg
 * @author Sarah Militana
 */

public class JiffyCal {

    public static void main(String[] args) throws IOException, Exception {
        LehighLoginDialog dialog = new LehighLoginDialog(new javax.swing.JFrame());
        dialog.Display();
    }
    
}
