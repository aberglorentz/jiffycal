package AppLogic;

/**
 * Basic extension of IOException to signify a login failure.
 * 
 * @author Lorentz Aberg
 * @author Sarah Militana
 */
public class LoginException extends java.io.IOException {
    
    // null constructor is implicit
    
    public LoginException (String message) {
        super(message);
    }
    
    public LoginException (String message, Throwable cause) {
        super(message, cause);
    }
    
    public LoginException (Throwable cause) {
        super(cause);
    }
}
