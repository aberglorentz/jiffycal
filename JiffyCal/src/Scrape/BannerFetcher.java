package Scrape;

import java.util.ArrayList;
import java.util.HashMap;
import java.net.URLEncoder;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import java.net.CookieHandler;
import java.net.CookieManager;

import AppLogic.LoginException;
import java.net.MalformedURLException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;


/**
 * Entry point for Scraper module.
 * 
 * Functionality is exposed through the getScheduleMarkup method. Any
 * functionality pursuant to fetching schedule markup should be private and
 * accessed through that static method only.
 * 
 * @author Lorentz Aberg
 * @author Sarah Militana
 */
public class BannerFetcher {
    
    // URLs and important strings for web access
    private static final String PORTAL_CONST_UUID = "0xACA021", // constant UUID form parameter passed with portal login
                                PORTAL_HOST_URL = "cp5.cc.lehigh.edu",
                                PORTAL_AUTHENTICATION_URL = "https://cp5.cc.lehigh.edu/cp/home/login",
                                PORTAL_REFERER_TO_AUTH_URL = "https://cp5.cc.lehigh.edu/login",
                                PORTAL_OKAY_PAGE_URL = "http://cp5.cc.lehigh.edu/cps/welcome/loginok.html",
                                PORTAL_REDIRECT_URL = "http://cp5.cc.lehigh.edu/cp/home/next",
                                PORTAL_HOME_URL = "http://cp5.cc.lehigh.edu/render.userLayoutRootNode.uP?uP_root=root",
                                BANNER_BUTTON_URL = "http://cp5.cc.lehigh.edu/cp/ip/timeout?sys=sctssb&url=https://lewisweb.cc.lehigh.edu/PROD/twbkwbis.P_GenMenu?name=bmenu.P_MainMnu",
                                BANNER_HOST_URL = "lewisweb.cc.lehigh.edu",
                                BANNER_TERM_SELECT_POST_URL = "https://lewisweb.cc.lehigh.edu/PROD/bwcklibs.P_StoreTerm",
                                BANNER_TERM_SELECT_URL = "https://lewisweb.cc.lehigh.edu/PROD/bwskflib.P_SelDefTerm",
                                BANNER_DETAIL_SCHEDULE_URL = "https://lewisweb.cc.lehigh.edu/PROD/bwskfshd.P_CrseSchdDetl",
                                BANNER_MAIN_MENU_URL = "https://lewisweb.cc.lehigh.edu/PROD/twbkwbis.P_GenMenu?name=bmenu.P_MainMnu&msg=WELCOME";
    
    // TDOO: fix this 
    
    /**
     * Fetches the HTML page for a user's schedule from Banner.
     * 
     * This is the only publicly-exposed method.
     * 
     * @return jsoup document containing the HTML fetched from user's schedule page
     * @throws AppLogic.LoginException
     */
    public static Document getScheduleMarkup (BannerSession session, String semesterID)
                           throws LoginException, IOException {
        
        String pageString; // stores GET response content
        
        // send POST request to select the appropriate semester 
        StringBuilder semesterSelectParams = new StringBuilder();
        semesterSelectParams.append("name_var=").append(URLEncoder.encode("bmenu.P_RegMnu", "UTF-8"));
        semesterSelectParams.append("&").append("term_in=").append(semesterID);
        
        try {
            int responseCode = RequestSender.sendPostRequest(BANNER_TERM_SELECT_POST_URL,
                                                             semesterSelectParams.toString(),
                                                             BANNER_HOST_URL,
                                                             BANNER_TERM_SELECT_URL, 
                                                             session);
        }
        catch (IOException e) {
            throw new IOException(("Banner term selection failed - GET request failed to " + BANNER_TERM_SELECT_POST_URL), e);
        }
        
        // finally pull down banner schedule page
        try {
            pageString = RequestSender.sendGetRequest(BANNER_DETAIL_SCHEDULE_URL,
                                                      BANNER_HOST_URL,
                                                      BANNER_MAIN_MENU_URL,
                                                      session);
        }
        catch (MalformedURLException e) {
            throw new IOException(("Banner schedule access failed - malformed URL passed at " + BANNER_DETAIL_SCHEDULE_URL), e);
        }
        catch (IOException e) {
            throw new IOException(("Banner schedule access - GET request failed to " + BANNER_DETAIL_SCHEDULE_URL), e);
        }
        
        // if no markup has been retrieved, throw an exception
        if (pageString.isEmpty()) {
            throw new IOException("Could not retrieve Banner schedule page");
        }
        else {
            return Jsoup.parse(pageString);
        }
    }
    
    /**
     * Accesses Banner's term select page and 
     * @param session
     * @throws LoginException
     * @return 
     */
    public static HashMap<String, String> getSemesters (BannerSession session) 
                                          throws IOException, LoginException {
        String pageString;  // storage variable for GET request results
        
        // get the term select page source
        try {
            pageString = RequestSender.sendGetRequest(BANNER_TERM_SELECT_URL,
                                                      BANNER_HOST_URL,
                                                      BANNER_MAIN_MENU_URL,
                                                      session);
        }
        catch (MalformedURLException e) {
            throw new IOException(("Banner term select access failed - malformed URL passed at " + BANNER_TERM_SELECT_URL), e);
        }
        catch (IOException e) {
            throw new IOException(("Banner term select access - GET request failed to " + BANNER_TERM_SELECT_URL), e);
        }
        
        // retrieve term IDs and return them in a hashmap
        Document termSelectPage = Jsoup.parse(pageString);
        
        Elements termOptions = termSelectPage.getElementsByTag("OPTION");  // get all OPTION elements - the dropdown list of semesters
        HashMap<String, String> semesterMap = new HashMap<>();
        for (Element element: termOptions) {                // iterate over all available semesters
            String semesterID = element.attr("VALUE");
            String semesterName = element.text();
            if (semesterName.contains("(View only)")) {
                semesterName = semesterName.substring(0, semesterName.indexOf("(View only)")); // remove "(View only)" text from semester name
            }
            
            semesterMap.put(semesterName, semesterID);
        }
        
        return semesterMap;
    }
    
    /**
     * Takes care of getting the Banner session cookies
     * @param session
     * @throws IOException
     * @throws LoginException 
     */
    private static void getBannerCookies (BannerSession session)
                        throws IOException, LoginException {
        
        String pageString; // storage variable for GET request results
        
        // access portal "next" page, retrieve necessary cookies
        try {
            pageString = RequestSender.sendGetRequest(PORTAL_REDIRECT_URL,
                                                      PORTAL_HOST_URL,
                                                      PORTAL_OKAY_PAGE_URL,
                                                      session);
        }
        catch (MalformedURLException e) {
            throw new IOException(("Portal login failed - malformed URL passed at " + PORTAL_REDIRECT_URL), e);
        }
        catch (IOException e) {
            throw new IOException(("Portal login failed - GET request failed to " + PORTAL_REDIRECT_URL), e);
        }
        
        // access portal main page, retrieve necessary cookies
        try {
            pageString = RequestSender.sendGetRequest(PORTAL_HOME_URL,
                                                      PORTAL_HOST_URL,
                                                      PORTAL_REDIRECT_URL,
                                                      session);
        }
        catch (MalformedURLException e) {
            throw new IOException(("Portal page access failed - malformed URL passed at " + PORTAL_HOME_URL), e);
        }
        catch (IOException e) {
            throw new IOException(("Portal page access - GET request failed to " + PORTAL_HOME_URL), e);
        }
        
        // access banner button URL, retrieve necessary cookies
        try {
            pageString = RequestSender.sendGetRequest(BANNER_BUTTON_URL,
                                                      PORTAL_HOST_URL,
                                                      PORTAL_HOME_URL,
                                                      session);
        }
        catch (MalformedURLException e) {
            throw new IOException(("Banner/Portal button page access failed - malformed URL passed at" + BANNER_BUTTON_URL), e);
        }
        catch (IOException e) {
            throw new IOException(("Banner/Portal button page access - GET request failed to " + BANNER_BUTTON_URL), e);
        }
        
        // extract banner redirect URL with session ID from javascript response
        int index = pageString.indexOf("https");
        
        // if we don't see the page we requested, the authentication has failed
        if (index == -1) {
            throw new LoginException("Banner authentication failed");
        }
        String bannerSessionRedirect = (pageString.substring(pageString.indexOf("https"))).split("\"")[0];
        
        // access banner session page, retrieve necessary cookies from *Banner* session
        try {
            pageString = RequestSender.sendGetRequest(bannerSessionRedirect,
                                                      BANNER_HOST_URL,
                                                      BANNER_BUTTON_URL,
                                                      session);
        }
        catch (MalformedURLException e) {
            throw new IOException(("Banner session acquisition failed - malformed URL passed at " + bannerSessionRedirect), e);
        }
        catch (IOException e) {
            throw new IOException(("Banner session acquisition - GET request failed to " + bannerSessionRedirect), e);
        }
    }
    
    /**
     * Handles logging into Portal (only) and getting all required session cookies.
     * 
     * @param username  the user's school username
     * @param password  the user's school password
     * @return
     * @throws AppLogic.LoginException 
     */
    public static BannerSession getBannerLogin (String username, String password)
                                throws LoginException {
        
        CookieHandler.setDefault(new CookieManager());
        
        BannerSession newSession = new BannerSession(); // Banner session to build and return
        
        // format the form parameters
        String formParameters;
        try {
            formParameters = formatPortalLoginParameters(username, password);
        }
        catch (UnsupportedEncodingException e) {
            throw new LoginException("Portal login failed - could not format POST parameters", e);
        }
        
        // send request, store cookies, and get the response code
        int responseCode;
        try {
            responseCode = RequestSender.sendPostRequest(PORTAL_AUTHENTICATION_URL,
                                                         formParameters,
                                                         PORTAL_HOST_URL,
                                                         PORTAL_REFERER_TO_AUTH_URL,
                                                         newSession);
        }
        catch (java.net.MalformedURLException e) {
            throw new LoginException("Portal login failed - malformed URL passed", e);
        }
        catch (IOException e) {
            throw new LoginException("Portal login failed - connection could not be made", e);
        }
        
        // if we received any status other than 200 - OKAY, throw an exception
        if (responseCode != 200) {
            throw new LoginException("Portal login failed - HTTP code " 
                                     + Integer.toString(responseCode) + " received");
        }
        
        try {
            getBannerCookies(newSession);
        }
        catch (IOException e) {
            throw new LoginException(e.getMessage(), e);
        }
        
        return newSession;
    }
    
    /**
     * Formats the portal login parameters as POST parameters
     * @param username Lehigh username
     * @param password Lehigh password
     * @return a formatted parameter string to be added to the POST request
     * @throws UnsupportedEncodingException should never be thrown, but required for
     * safety reasons
     */
    private static String formatPortalLoginParameters (String username, String password)
                   throws UnsupportedEncodingException {
        
        // these parameters are hardcoded for current Portal login requirements
        ArrayList<String> paramList = new ArrayList<>();
        
        paramList.add("user=" + URLEncoder.encode(username, "UTF-8"));
        paramList.add("pass=" + URLEncoder.encode(password, "UTF-8"));
        paramList.add("uuid=" + URLEncoder.encode(PORTAL_CONST_UUID, "UTF-8"));
        
        // build form parameters
        StringBuilder paramString = new StringBuilder();
        for (String parameter: paramList) {
            if (paramString.length() == 0) {
                paramString.append(parameter);
            }
            else {
                paramString.append("&").append(parameter);
            }
        }
        
        return paramString.toString();
    }
    
}