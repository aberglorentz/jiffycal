package Scrape;

import java.util.List;
import java.io.DataOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;

/**
 * Handles sending and receiving HTTP requests
 * 
 * @author Lorentz Aberg
 * @author Sarah Militana
 */
public class RequestSender {
    
    private static final String USER_AGENT_STRING = "Mozilla/5.0";
    
    /**
     * Sends an HTTP form POST request. 
     * 
     * The main purpose of this method is to fetch cookies generated from a login.
     * The passed BannerSession object is modified to add any cookies sent by the
     * server.
     * 
     * @param url  the URL to send the POST request to
     * @param postParams  the parameters to be passed, in form (param=value)[&param=value]*
     * @param host  the host URL to be reported to the server
     * @param referer  the referer URL to be reported to the server
     * @param session  BannerSession object containing any cookies to be sent, 
     *                 and to which any generated cookies should be added
     * @return The server response HTTP status code
     * @throws java.net.MalformedURLException
     * @throws IOException  if the request fails for any reason other than a malformed URL
     */
    public static int sendPostRequest (String url,
                                       String postParams,
                                       String host,
                                       String referer,
                                       BannerSession session) 
                                      throws MalformedURLException,
                                              IOException {
        
        URL serviceURL = new URL(url);
        HttpURLConnection connection = (HttpsURLConnection) serviceURL.openConnection();
        
        // set up request parameters and connection environment
        connection.setUseCaches(false);
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("User-Agent", USER_AGENT_STRING);
        connection.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        connection.setRequestProperty("Connection", "keep-alive");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        
        // add cookies, if any, to the request properties
        List<String> cookies = session.getCookies();
        for (String cookie: cookies) {
            connection.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
        }
        
        // set user-defined request properties
        connection.setRequestProperty("Host", host);
        connection.setRequestProperty("Referer", referer);
        connection.setRequestProperty("Content-Length", Integer.toString(postParams.length()));
        
        connection.setDoOutput(true);
        connection.setDoInput(true);
        
        // send the actual request
        try (DataOutputStream writeStream = new DataOutputStream(connection.getOutputStream())) {
            writeStream.writeBytes(postParams);
            writeStream.flush();
        }
        catch (Exception e) {
            throw e;
        }
        
        
        int responseCode = connection.getResponseCode();
        
        if (responseCode == 200) {
            List<String> newCookies = connection.getHeaderFields().get("Set-Cookie");
            if (newCookies != null) {
                session.addCookies(newCookies);
            }
        }
        
        connection.disconnect();
        return responseCode;
    }
    
    /**
     * Sends an HTTP GET request
     * 
     * @param url  the URL to send the GET request to
     * @param host  the host URL to be reported to the server
     * @param referer  the referer URL to be reported to the server
     * @param session  BannerSession object containing any cookies to be sent,
     *                 and to which any generated cookies should be added
     * @return the response content from the GET request (should be HTML markup)
     * @throws MalformedURLException
     * @throws IOException  if the request fails for any reason other than a malformed URL
     */
    public static String sendGetRequest (String url,
                                         String host,
                                         String referer,
                                         BannerSession session) 
                                        throws MalformedURLException,
                                               IOException {
        
        URL serviceURL = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) serviceURL.openConnection();
        
        // set up request parameters and connection environment
        connection.setUseCaches(false);
        connection.setDoOutput(true);
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", USER_AGENT_STRING);
        connection.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        connection.setRequestProperty("Connection", "keep-alive");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        
        // add cookies, if any, to the request properties
        List<String> cookies = session.getCookies();
        for (String cookie: cookies) {
            connection.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
        }
        
        // set user-defined request properties
        connection.setRequestProperty("Host", host);
        connection.setRequestProperty("Referer", referer);
        
        // send request and get page content
        StringBuilder response = new StringBuilder();
        String inputLine;
        
        try (BufferedReader responseReader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            while ( (inputLine = responseReader.readLine()) != null) {
                response.append(inputLine);
            }
        }
        
        List<String> newCookies = connection.getHeaderFields().get("Set-Cookie");
        if (newCookies != null) {
            session.addCookies(newCookies);
        }
        
        connection.disconnect();
        return response.toString();
    }
}
