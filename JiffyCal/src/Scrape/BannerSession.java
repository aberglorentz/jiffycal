package Scrape;

import java.util.ArrayList;
import java.util.List;

/**
 * Stores Banner session data (i.e. cookies)
 * All cookies are stored regardless of original domain
 * 
 * @author Lorentz Aberg
 * @author Sarah Militana
 */
public class BannerSession {

    private List<String> cookies;

    /**
     * Basic constructor. 
     * 
     * Initializes the internal list of cookies
     */
    public BannerSession () {
        cookies = new ArrayList<>();
    }

    /**
     * Gets the session's list of cookies.
     * 
     * @return List of cookies. Cookies are in the standard form "name=value"
     */
    public List<String> getCookies () {
        return cookies;
    }

    /**
     * Adds a list of cookies to the session's list of cookies
     * 
     * @param cookies  a list of cookies to add
     */
    public void addCookies (List<String> cookies) {
        for (String cookieString: cookies) {
            this.cookies.add(cookieString);
        }
    }
    
    /**
     * Adds a cookie to the session's list of cookies
     * 
     * @param cookie  the cookie to add
     */
    public void addCookie (String cookie) {
        this.cookies.add(cookie);
    }

}
