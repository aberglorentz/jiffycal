package Parse;

import org.joda.time.LocalTime;
import org.joda.time.LocalDate;

/**
 * Basic class to contain information about a particular class meeting
 * @author Lorentz Aberg
 * @author Sarah Militana
 */
public class ClassMeeting {
    
    // all members are public because access to each will be
    public enum DayOfWeek {MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY};
    
    public final LocalTime startTime, endTime;
    public final LocalDate firstDay, lastDay;
    
    public final DayOfWeek day;
    
    public final String classNumber, classSection, className, buildingName, roomNumber, professorName, professorEmail;
    
    /**
     * Basic constructor, ensuring that all members are populated at time of instantiation
     */
    public ClassMeeting (String name,
                         String classNumber,
                         String classSection,
                         LocalTime startTime,
                         LocalTime endTime,
                         DayOfWeek day,
                         String buildingName,
                         String roomNumber,
                         String professorName,
                         String professorEmail,
                         LocalDate firstDay,
                         LocalDate lastDay) {
        this.className = name;
        this.classNumber = classNumber;
        this.classSection = classSection;
        this.startTime = startTime;
        this.endTime = endTime;
        this.day = day;
        this.buildingName = buildingName;
        this.roomNumber = roomNumber;
        this.professorName = professorName;
        this.professorEmail = professorEmail;
        this.firstDay = firstDay;
        this.lastDay = lastDay;
    }
    
    /**
     * Basic converter to get a string value from a dayofweek enum
     * @param day the day of the week as an enum
     * @return String representation of the day of the week
     */
    public static final String dayToString (DayOfWeek day) {
        String returnValue = null;
        switch (day) {
            case MONDAY: returnValue = "Monday"; break;
            case TUESDAY: returnValue = "Tuesday"; break;
            case WEDNESDAY: returnValue = "Wednesday"; break;
            case THURSDAY: returnValue = "Thursday"; break;
            case FRIDAY: returnValue =  "Friday"; break;
            case SATURDAY: returnValue = "Saturday"; break;
            case SUNDAY: returnValue = "Sunday"; break;
        }
        
        return returnValue;
   }
    
}
