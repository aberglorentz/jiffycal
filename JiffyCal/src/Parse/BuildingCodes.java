package Parse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.nio.charset.Charset;

/**
 * Singleton class that translates two-letter building codes into their full
 * names. Static members were considered, but since it requires loading in a
 * file the singleton pattern made more sense to make sure it was initialized.
 * 
 * AS OF NOW, THIS CLASS IS DEPRECATED AND UNUSED
 * A different format for schedule access has made this unnecessary for the 
 * forseeable future. 
 * 
 * @author Lorentz Aberg
 * @author Sarah Militana
 * @deprecated
 */
public class BuildingCodes {
    
    private static BuildingCodes instance;
    
    private HashMap<String, String> buildingCodeMap;
    
    public static BuildingCodes getInstance () throws IOException, FileNotFoundException {
        if (instance == null) {
            instance = new BuildingCodes();
        }
        
        return instance;
    }
    
    private BuildingCodes () throws IOException, FileNotFoundException {
        // URL comes web-encoded, with the file: prefix (basically formatted for web browser)
        String buildingFilePath = java.net.URLDecoder.decode((ClassLoader.getSystemResource("BuildingCodes.txt").toString()),
                                                             Charset.defaultCharset().toString());
        buildingFilePath = buildingFilePath.substring(5); // remove "file:" from beginning of URL
        File buildingFile = new File(buildingFilePath); 
       BufferedReader fileReader = new BufferedReader(new FileReader(buildingFile));
        
        String line;
        buildingCodeMap = new HashMap<>();
        while ((line = fileReader.readLine()) != null) {
            String abbreviation = line.substring(0,line.indexOf(' '));
            String name = line.substring(line.indexOf(' ') + 1);
            
            buildingCodeMap.put(abbreviation, name);
        }
        
        if (buildingCodeMap.isEmpty()) {
            buildingCodeMap = null;
            throw new IOException("Could not read BuildingCodes.txt");
        }
    }
    
    public String getBuildingCode (String abbreviation) {
        return buildingCodeMap.get(abbreviation);
    }
    
}
