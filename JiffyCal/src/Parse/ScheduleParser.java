package Parse;

import org.jsoup.nodes.*;
import org.jsoup.select.*;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import Parse.SemesterSchedule.Seasons;
import Parse.ClassMeeting.DayOfWeek;

/**
 * Parses a jsoup Document of class schedule detail.
 * 
 * @author Lorentz Aberg
 * @author Sarah Militana
 */
public class ScheduleParser {
    
    public static SemesterSchedule parseSchedule (Document bannerSchedule) {
        
        SemesterSchedule newSchedule = new SemesterSchedule(Seasons.FALL, 2014, new LocalDate("2014-11-6"));
        
        Elements dataTables = bannerSchedule.select(".datadisplaytable");
        
        /*
         * each course has two tables that provide information - a general table
         * with registrar-centric information, and another one with the more
         * day-to-day details. these alternate, so grouping them in groups of 2
         * is natural. 
         * 
         * they will be contained in the ArrayList courseTables in 2-item arrays
         */
        ArrayList<Element[]> courseTables = new ArrayList<>();
        boolean createArray = true; // when createArray is true, a new array is allocated
        for (Element e: dataTables) {
            if (createArray) {
                Element[] newArray = new Element[2];
                newArray[0] = e;
                courseTables.add(newArray);
            }
            else {
                (courseTables.get(courseTables.size() - 1))[1] = e;
            }
            createArray = !(createArray || false);  // toggle arrayCreate via NOR
        }
        
        // at this point each item in the courseTables ArrayList represents a course
        // so we can iterate through the list and do operations per course
        for (Element[] courseArray: courseTables) {
            
            String className,
                   classNum,
                   classSection;
                   
            // extract course name, number, and section first from caption above first table
            String captionText = courseArray[0].select("caption").get(0).text();
            String[] captionContents = captionText.split(" - ");
            className = captionContents[0];
            classNum = captionContents[1];
            classSection = captionContents[2];
            
            // extract the rest of the data from the second table
            Elements rows = courseArray[1].select("tr");
            
            // each secondary table has seven columns:
            // type, time, days, where, date range, schdule type, and instructor
            for (Element row: rows) {
                List<Element> rowEntries = row.select(".dddefault");
                
                // variables for class information. these are all initialized to null
                // in order to avoid compiler/IDE problems - they will always have
                // real values assigned to them by the time they are needed
                String weekDayString = "";   // the string representation of days the class meets
                String buildingName = null, room = null, instructor = null, instructorEmail = null;
                LocalTime startTime = null, endTime = null;
                LocalDate firstDay = null, lastDay = null;
                DateTimeFormatter timeFormat = DateTimeFormat.forPattern("hh:mm aa");
                DateTimeFormatter dateFormat = DateTimeFormat.forPattern("MMM dd, yyyy");
                
                int i = 0; // keeps track of which element in the row we're at
                for (Element entry: rowEntries) {
                    switch (i) {   // seven entries, we only want data from some
                        case 0: break; // type, usually "class" - not useful
                        case 1: // meeting time, start and finish
                                // formatted as "hh:mm aa - hh:mm aa"
                                String[] times = entry.text().split(" - ");
                                
                                // if class meeting time is marked as "TBA," set
                                // starting time and ending time to midnight
                                if (times[0].equals("TBA")) {
                                    startTime = timeFormat.parseLocalTime("12:00 AM");
                                    endTime = timeFormat.parseLocalTime("12:00 AM");
                                }
                                else {
                                    startTime = timeFormat.parseLocalTime(times[0]);
                                    endTime = timeFormat.parseLocalTime(times[1]);
                                }
                                break;
                        case 2: // days the class meets
                                // a string where each character represents a day
                                // in order, MTWRF (Thursday is "R")
                                weekDayString = entry.text();
                                break;
                        case 3: // class location
                                // if the location is TBA, set location accordingly
                                String testString = entry.text().toLowerCase();
                                if (testString.contains("determined") || testString.contains("announced") 
                                                     || testString.contains("to be") || testString.contains("tba")) {
                                    room = "";
                                    buildingName = "TBA";
                                }
                                // assuming a classroom is specified, read it in the normal manner
                                // formatted like "000 Building Name 000", where the first number
                                // is useless to us and the final group is the room number
                                else {
                                    String buildingRoomString = entry.text().substring(entry.text().indexOf(" ") + 1);
                                    int splitIndex = buildingRoomString.lastIndexOf(" ");
                                    room = buildingRoomString.substring(splitIndex + 1);
                                    buildingName = buildingRoomString.substring(0, splitIndex);
                                }
                                break;
                        case 4: // determine starting & ending dates
                                // formatted like "Aug 25, 2014 - Dec 05, 2014"
                                String dateString = entry.text();
                                String[] dates = dateString.split(" - ");
                                firstDay = dateFormat.parseLocalDate(dates[0]);
                                lastDay = dateFormat.parseLocalDate(dates[1]);
                                break;
                        case 5: break; // schedule type - not useful
                        case 6: // instructor name and email link
                                // instructor text ends with (P) if they are the primary instructor
                                String instructorText = entry.text();
                                int parenIndex = instructorText.indexOf("(P)");
                                if (parenIndex > -1) {
                                    instructor = instructorText.substring(0, parenIndex - 1);
                                }
                                else {
                                    instructor = instructorText;
                                }
                                
                                // if there is no email associated with the professor (teaching staff, TBA), set email to be empty
                                if (entry.select("a").isEmpty()) {
                                    instructorEmail = "";
                                }
                                else {
                                    Element email = entry.select("a").get(0); // get email link element
                                    String emailText = email.attr("href"); // get the mailto: link
                                    instructorEmail = emailText.substring(emailText.indexOf(":") + 1); // strip mailto: from email
                                }
                                break;
                    }
                    i++; // increment to access next entry properly
                    
                }
                
                // create the new ClassMeeting object
                    
                for (int n = 0; n < weekDayString.length(); n++) {
                    DayOfWeek day = null;
                    char c = weekDayString.charAt(n);
                    switch (c) {
                        case 'M':
                        case 'm': day = DayOfWeek.MONDAY;
                                  break;
                        case 'T':
                        case 't': day = DayOfWeek.TUESDAY;
                                  break;
                        case 'W':
                        case 'w': day = DayOfWeek.WEDNESDAY;
                                  break;
                        case 'R':
                        case 'r': day = DayOfWeek.THURSDAY;
                                  break;
                        case 'F':
                        case 'f': day = DayOfWeek.FRIDAY;
                                  break;
                    }
                    
                    newSchedule.addClass(new ClassMeeting(className,
                                                          classNum,
                                                          classSection,
                                                          startTime,
                                                          endTime,
                                                          day,
                                                          buildingName,
                                                          room,
                                                          instructor,
                                                          instructorEmail,
                                                          firstDay,
                                                          lastDay) );
                }
            }
        }
        
        return newSchedule;
    }
    
}
