package Parse;

import org.joda.time.LocalDate;
import java.util.HashMap;
import java.util.ArrayList;
import Parse.ClassMeeting.DayOfWeek;

// imports for printing the schedule
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Contains information about a given semester, and access to a list of class
 * meetings for each weekday. 
 * @author Lorentz Aberg
 * @author Sarah Militana
 */
public class SemesterSchedule {
    
    public enum Seasons {SUMMER, FALL, SPRING, WINTER};
    
    protected final Seasons season;
    
    protected final int year;
    
    protected int numClasses;
    
    protected final LocalDate firstBreakDay;
    
    // contains a HashMap indexed by the days of the week
    // each day of the week points to an arraylist of class meetings on that day
    private HashMap< DayOfWeek, ArrayList<ClassMeeting> > classes;
    
    /**
     * Creates a new SemesterSchedule with the appropriate information
     */
    public SemesterSchedule (Seasons season,
                             int year,
                             LocalDate firstBreakDay) {
        
        this.season = season;
        this.year = year;
        this.firstBreakDay = firstBreakDay;
        
        numClasses = 0;
        
        // create a new arraylist for each day of the week
        classes = new HashMap<>();
        for (DayOfWeek day: DayOfWeek.values()) {
            classes.put(day, new ArrayList<ClassMeeting>());
        }
    }
    
    /**
     * Adds a class to the semester's schedule
     * @param classMeeting a class meeting to be added
     * @return updated SemesterSchedule object to allow schedule.add(class1).add(class2) constructions
     */
    public SemesterSchedule addClass (ClassMeeting classMeeting) {
        if (classMeeting.day != null) {    // classes that don't have a day to meet can't be displayed on a calendar, so don't add them
            classes.get(classMeeting.day).add(classMeeting);
            numClasses++;
        }
        
        return this;
    }
    
    /**
     * Simple debugging method to display contents of the schedule in the console.
     * Helpful for demos of parsing functionality.
     */
    public void printSchedule () {
        DateTimeFormatter dateFormat = DateTimeFormat.forPattern("MMM d, YYYY");
        DateTimeFormatter timeFormat = DateTimeFormat.forPattern("hh:mm aa");
        
        System.out.println("Schedule for semester: " + seasonToString(season) + " " + year);
        System.out.println("  " + "First day of session break: " + dateFormat.print(firstBreakDay));
        System.out.println();
        
        for (DayOfWeek weekDay: DayOfWeek.values()) {
            ArrayList<ClassMeeting> dayClasses  = classes.get(weekDay);
            System.out.println("  " + ClassMeeting.dayToString(weekDay) + " classes:");
            
            for (ClassMeeting classMeeting: dayClasses) {
                System.out.println("    " + classMeeting.className);
                System.out.println("      " + classMeeting.classNumber + " - " + classMeeting.classSection);
                System.out.println("      " + "Professor: " + classMeeting.professorName + " (" + classMeeting.professorEmail + ")");
                System.out.println("      " + classMeeting.buildingName + " " + classMeeting.roomNumber);
                System.out.println("      " + "Meets " + timeFormat.print(classMeeting.startTime) + " - " + timeFormat.print(classMeeting.endTime));
                System.out.println();
            }
            
            if (dayClasses.size() == 0) {
                System.out.println();
            }
        }
    }
    
    /**
     * Simple converter from a season enum to the string representation
     */
    public static final String seasonToString (Seasons season) {
        String returnValue = null;
        switch (season) {
            case FALL: returnValue = "Fall"; break;
            case SPRING: returnValue = "Spring"; break;
            case WINTER: returnValue = "Winter"; break;
            case SUMMER: returnValue = "Summer"; break;
        }
        
        return returnValue;
    }
    
    // accessors for the private members follow
    
    public int getYear () {
        return year;
    }
    
    public Seasons getSeason () {
        return season;
    }
    
    public LocalDate getFirstBreakDay () {
        return firstBreakDay;
    }
    
    public final HashMap< DayOfWeek, ArrayList<ClassMeeting> > getClasses () {
        return classes;
    }
    
    public boolean hasClasses () {
        return (numClasses > 0);
    }
}
